README - Junior PHP Test for Sam Fullen - 08/03/16 

Hi Tom,
Thanks for viewing my work. I hope it meets the requirements set in the test parameters. 

I used xampp for my apache web server and my MySQL server, in order for you to save into the database and not receive fatal database errors you should set up a database called php test, within that there needs to be a table made called users which holds 9 columns:
id INT Primary Key Unsigned
firstname VARCHAR (40)
surname VARCHAR (40)
email VARCHAR (40)
phone INT (40)
addressln1 VARCHAR (40)
addressln2 VARCHAR (40)
postcode VARCHAR (8)
timestamp

Once this is set up you should be good to add users into the database then view them on the show users page.

For the validation:
All fields must be filled in.
First Name - Validated so user cannot enter any more than 40 characters as well as not being able to enter any numeric values.
Surname - Same as above.
Email - validated so it must contain an @symbol, I have used a regular expression for this, however it is easier to use filter_var($email, FILTER_VALIDATE_EMAIL), I only used regex as it specified so in the test specification. The email cannot be any longer than 40 chars.
Phone - Must be a numeric value and as per UK mobile numbers must be between 7-11 characters.
Address Line 1 & 2 - Can be no more than 40 characters.
Postcode - Stripped of all spaces, must be between 5-7 chars (without spaces).

Furthermore, the fields have all been protected using htmlspecialchars();

If you require any more information please feel free to ring me on 07816223236 or email me at sam_fullen2@hotmail.co.uk. It was a pleasure working with you today,

Kind Regards,
Sam.