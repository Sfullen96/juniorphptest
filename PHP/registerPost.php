<?php 

	$firstname = $_POST['firstName'];
	$lastname = $_POST['surname'];
	$email = $_POST['email'];
	$phone = $_POST['phone'];
	$addressln1 = $_POST['addressLn1'];
	$addressln2 = $_POST['addressLn2'];
	$postcode = $_POST['postcode'];



	// First, check if all the fields have been submitted.
	if(isset($firstname) && isset($lastname) && isset($email) && isset($phone) && isset($addressln1) && isset($addressln2) && isset($postcode)){
		// Next, check if the fields have some data in them i.e. not empty.
		if(!empty($firstname) && !empty($lastname) && !empty($email) && !empty($phone) && !empty($addressln1) && !empty($addressln2) && !empty($postcode)){
			registerUser();
		} else {
			include 'config.php';
			$_SESSION['err'] = "All fields must be filled in!";
			header("Location: index.php");
		}
	} 

	function registerUser(){
		include 'config.php';
		// Using HTML entities to stop any HTML chars being used
		$firstname = htmlentities($_POST['firstName']);
		$lastname = htmlentities($_POST['surname']);
		$email = htmlentities($_POST['email']);
		$phone = htmlentities($_POST['phone']);
		$addressln1 = htmlentities($_POST['addressLn1']);
		$addressln2 = htmlentities($_POST['addressLn2']);
		$postcode = htmlentities($_POST['postcode']);

		// Count all fields

		// Count first and last names 
		$count_first_name = strlen($firstname);
		$count_surname = strlen($lastname);
		// Count Phone
		$count_phone = strlen($phone);
		// Count the characters in the address lines
		$address_count1 = strlen($addressln1);
		$address_count2 = strlen($addressln2);
		// Count Postcode
		$count_postcode = strlen($postcode);

		// Check email has an @ symbol
		$email_validate = preg_match('/^([a-z0-9]+([_\.\-]{1}[a-z0-9]+)*){1}([@]){1}([a-z0-9]+([_\-]{1}[a-z0-9]+)*)+(([\.]{1}[a-z]{2,6}){0,3}){1}$/i', $email);

		// Check phone number only contains numbers
		// Phone number should be between 8 and 11 with the 0, stripping
		// all spaces from number.
		$phone_nospaces = str_replace(' ', '', $phone);
		$phone_validate = preg_match("/^[1-9][0-9]*$/", $phone);
		
		$firstname_validate = !preg_match("/^[a-zA-Z'-]+$/",$firstname);
		$surname_validate = !preg_match("/^[a-zA-Z'-]+$/",$lastname);

		// Strip the postcode of any spaces, then count the characters in the string
		// UK Postcodes are 6-8 characters, 5-7 without spaces, so need to make sure
		// strlen is less than or equal to between 5 & 7.
		$postcode = str_replace(' ', '', $postcode);

		// Error messages
		if($count_postcode >= 7 || $count_postcode <= 5){
			$_SESSION['err'] = 'Your postcode should be between 5 and 8 characters!';
			header("Location: http://localhost/JuniorPHPTestSamFullen/PHP/index.php");
		} else if($address_count2 >= 40){
			$_SESSION['err'] = 'Your address line 2 should be less than 40 characters';
			header("Location: http://localhost/JuniorPHPTestSamFullen/PHP/index.php");
		} else if($address_count1 >= 40){
			$_SESSION['err'] = 'Your address line 1 should be less than 40 characters';
			header("Location: http://localhost/JuniorPHPTestSamFullen/PHP/index.php");
		}  else if(!$phone_validate){
			$_SESSION['err'] = 'Your phone number should be between 7 & 11 digits and contain no letters';
			header("Location: http://localhost/JuniorPHPTestSamFullen/PHP/index.php");
		} else if(!$email_validate){
			$_SESSION['err'] = 'Your email address is not formatted correctly!';
			header("Location: http://localhost/JuniorPHPTestSamFullen/PHP/index.php");
		} else if($count_surname >= 40 && $surname_validate){
			$_SESSION['err'] = 'Your surname must be under 40 characters & cannot contain numbers!';
			header("Location: http://localhost/JuniorPHPTestSamFullen/PHP/index.php");
		} else if($count_first_name >= 40 || $firstname_validate){
			$_SESSION['err'] = 'Your first name must be under 40 characters & cannot contain numbers!';
			header("Location: http://localhost/JuniorPHPTestSamFullen/PHP/index.php");
		} else {
			$query = "INSERT INTO users (firstname, surname, email, phone, addressln1, addressln2, postcode)
			VALUES ('$firstname', '$lastname', '$email', '$phone', '$addressln1', '$addressln2', '$postcode')";
			$conn->exec($query);
			$_SESSION['success'] = "Successfully registered," . '<a href="showUsers.php">' . "click here to view users!" . '</a>';
			header("Location: http://localhost/JuniorPHPTestSamFullen/PHP/index.php");
		}		
	}
?>
