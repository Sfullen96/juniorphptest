<?php 
	
	/** 
	Database Settings	
	**/
	ob_start();
	@session_start();

	$db_host = 'localhost';
	$db_user = 'root';
	$db_password = '';
	$db_name = 'phptest';

	try {
	    $conn = new PDO("mysql:host=$db_host;dbname=$db_name", $db_user, $db_password);
	    // set the PDO error mode to exception
	    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
		catch(PDOException $e)
    {
   	 	
    }


 	if(isset($_SERVER['HTTP_REFERER'])){
 		$http_referer = $_SERVER['HTTP_REFERER'];
	}

 	$curr_file = $_SERVER['SCRIPT_NAME'];
	$host = $_SERVER['HTTP_HOST'];

	$image_path = '../Images/';

	// Getting client IP

	$http_client_ip = @$_SERVER['HTTP_CLIENT_IP']; // Get Client IP not shared 192 IP

	$http_x_forwarded_for = @$_SERVER['HTTP_X_FORWARDED_FOR']; // If user is using a proxy

	$remote_addr = @$_SERVER['REMOTE_ADDR']; // Not as reliable, last resort if other 2 fail

	if(!empty($http_client_ip)){
		$user_ip = $http_client_ip;
	} else if(!empty($http_x_forwarded_for)){
		$user_ip = $http_x_forwarded_for;
	} else {
		$user_ip = $remote_addr;
	} 
	echo "<script> console.log('$user_ip'); </script>";
?>