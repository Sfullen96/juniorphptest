<!DOCTYPE html>
<html>
	<head>
		<title>Sam Fullen - Home Page</title>
	</head>
	<body>
		<?php require_once 'navbar.php';
		?>
		<div class="container">
			<form action="registerPost.php" method="POST" role="form" class="registerForm">
				<legend> Register a New User </legend>
				<div class="errorMessage marginBottom">
					<?php
						if(isset($_SESSION['err'])){
							echo $_SESSION['err'];
							unset($_SESSION['err']);
						}
					?>
				</div>
				<div class="successMessage marginBottom">
				<?php 
					if(isset($_SESSION['success'])){
						echo $_SESSION['success'];
						unset($_SESSION['success']);
					}
				?>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-xs-6">
							<input type="text" class="form-control marginBottom" name="firstName" placeholder="First Name">
						</div>
						<div class="col-xs-6">
							<input type="text" class="form-control marginBottom" name="surname" placeholder="Surname">
						</div>
					</div>
					<input type="text" class="form-control marginBottom" name="email" placeholder="Email">
					<input type="text" class="form-control marginBottom" name="phone" placeholder="Telephone Number">
					<input type="text" class="form-control marginBottom" name="addressLn1" placeholder="Address Line 1">
					<input type="text" class="form-control marginBottom" name="addressLn2" placeholder="Address Line 2">
					<input type="text" class="form-control marginBottom" name="postcode" placeholder="Postcode">
				</div>
				<button type="submit" class="btn btn-primary">Submit</button>
				<a class="btn btn-success"> View Users </a>
			</form>
		</div>
		<?php require_once 'footer.php' ?>
	</body>
</html>