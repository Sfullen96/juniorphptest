<!DOCTYPE html>
<html>
	<head>
		<title>Sam Fullen - Home Page</title>
	</head>
	<body>
		<?php require_once 'navbar.php'; ?>
		<?php
			class TableRows extends RecursiveIteratorIterator {
			function __construct($it) {
			parent::__construct($it, self::LEAVES_ONLY);
			}
			function current() {
			return "<td>" . parent::current(). "</td>";
			}
			function beginChildren() {
			echo "<tr>";
					}
					function endChildren() {
			echo "</tr>" . "\n";
			}
			}
			function displayUser(){
				include 'config.php';
			try {
			$query = $conn->prepare("SELECT id, firstname, surname, email, phone, addressln1, addressln2, postcode, timestamp FROM users");
			$query->execute();
			// set the resulting array to associative
			$result = $query->setFetchMode(PDO::FETCH_ASSOC);
			foreach(new TableRows(new RecursiveArrayIterator($query->fetchAll())) as $k=>$v) {
			echo $v;
			}
			}
			catch(PDOException $e) {
			echo "Error: " . $e->getMessage();
			}
			$conn = null;
		}
		?>
		<div class="container">
			<table class="table table-hover">
				<thead>
					<tr style="font-weight:bold">
						<td> ID </td>
						<td> First Name </td>
						<td> Last Name </td>
						<td> Email </td>
						<td> Phone </td>
						<td> Address Line 1 </td>
						<td> Address Line 2 </td>
						<td> Postcode </td>
						<td> User Created at </td>
					</tr>
				</thead>
				<tbody>
					<?php
						if(isset($_POST['show'])){
							displayUser();
						}
					?>
				</tbody>
			</table>
			<form action="showUsers.php" method="POST" role="form">
				<button type="submit" name="show" class="btn btn-primary">Show Users </button>
			</form>
		</div>
		<?php require_once 'footer.php'; ?>
	</body>
</html>