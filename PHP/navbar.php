<head>
	<link rel="stylesheet" type="text/css" href="../CSS/Bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../CSS/main.css">
	<link rel="stylesheet" type="text/css" href="../CSS/MobileCSS/mobile.css">
	<?php  require 'config.php'; ?>
</head>
<div class="wrapper"> <!-- Wrapper Start, wraps all content except the footer -->
<nav class="navbar navbar-default" role="navigation">
	<div class="container-fluid">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="index.php"> Home </a>
		</div>
		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse navbar-ex1-collapse">
			<ul class="nav navbar-nav">
				<li><a href="showUsers.php"> Show users </a></li>
				<li><a href="index.php"> Register a User </a></li>
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<li><a href="login.php"> login </a></li>
			</ul>
			</div><!-- /.navbar-collapse -->
		</div>
	</nav>
	<div class="mainContent">