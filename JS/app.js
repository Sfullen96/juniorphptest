$(function() {
	//Toggle click Jquery plugin
	$.fn.toggleClick = function() {
		var methods = arguments, // store the passed arguments for future reference
			count = methods.length; // cache the number of methods 

		//use return this to maintain jQuery chainability
		return this.each(function(i, item) {
			// for each element you bind to
			var index = 0; // create a local counter for that element
			$(item).click(function() { // bind a click handler to that element
				return methods[index++ % count].apply(this, arguments); // that when called will apply the 'index'th method to that element
				// the index % count means that we constrain our iterator between 0 and (count-1)
			});
		});
	};

	// Code for jumbotron image fading
	$(function() {
		$('.fadein img:gt(0)').hide(); // hide the first image
		setInterval(function() {
				$('.fadein :first-child').fadeOut(2000) // Fade in the next image over 3 seconds
				.next('img').fadeIn()
					.end().appendTo('.fadein');
			},
			7000); // Wait 7 seconds before switching to next
	});

	// Code for homepage links
	$('#aboutMe').hover(
		function() {
			$(this).find("#profileImg").stop(true, false).fadeOut(800);
		},
		function() {
			$(this).find("#profileImg").stop(false, true).fadeIn(800);
		}
	);

	$('#contact').hover(
		function() {
			$(this).find("#contactImg").stop(true, false).fadeOut(800);
		},
		function() {
			$(this).find("#contactImg").stop(false, true).fadeIn(800);
		}
	);

	$('#portfolio').hover(
		function() {
			$(this).find("#portfolioImg").stop(false, false).fadeOut(800);
		},
		function() {
			$(this).find("#portfolioImg").stop(false, true).fadeIn(800);
		}
	);

	$('#blog').hover(
		function() {
			$(this).find("#blogImg").stop(false, false).fadeOut(800);
		},
		function() {
			$(this).find("#blogImg").stop(false, true).fadeIn(800);
		}
	);

	$('.homeLink').hover(
		function() {
			$(this).stop(false, false).animate({
				fontSize: '22px',
			});
		},
		function() {
			$(this).stop(false, false).animate({
				fontSize: '20px',
			});
		}
	);

});

// Angular code that controls the validation on the register forms
var app = angular.module('tctraining', []);
app.controller('validateCtrl', function($scope) {
	$scope.next = false;
	$scope.movenext = function() {
		$scope.next = true;
	}
	$scope.prev = function() {
		$scope.next = false;
	}
});
app.controller('validateBusinessCtrl', function($scope) {
	$scope.next = false;
	$scope.movenext = function() {
		$scope.next = true;
	}
	$scope.prev = function() {
		$scope.next = false;
	}
});